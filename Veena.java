import java.util.HashMap;
import java.util.*;
public class Veena {
    int marks;
    int roll;
    public Veena(int roll,int marks){
        this.roll=roll;
        this.marks=marks;
    }
    public String toString(){
        return this.roll+" "+this.marks;
    }
    public static class Sortbyroll implements Comparator<Veena>
    {

        public int compare(Veena i1, Veena i2)
        {
            if (i1.marks== i2.marks)
                return 0;
            else if (i1.marks < i2.marks)
                return 1;
            else
                return -1;
        }
    }
    public static void main(String args[]){
        HashMap<String, ArrayList<Integer>> students=new HashMap<>();
        System.out.println("Enter studentname,Physics,Chemistry,Maths,CS marks");
        Scanner scan=new Scanner(System.in);
        HashMap<String, ArrayList<Integer>> deptwise = new HashMap<>();

        ArrayList<Veena> ranks=new ArrayList<>();
        ArrayList<Integer> cse = new ArrayList<>();
        ArrayList<Integer> bio = new ArrayList<>();
        ArrayList<Integer> comm = new ArrayList<>();
        for(int i=0;i<3;i++) {
            int roll = scan.nextInt();
            int Physics = scan.nextInt();
            int Chemistry = scan.nextInt();
            int Maths = scan.nextInt();
            int CS = scan.nextInt();
            int sum=Physics+Chemistry+Maths+CS;
            ranks.add(new Veena(roll,sum));

            if (Physics > 70 && Chemistry > 70 && Maths > 70 && CS > 80) {
                cse.add(roll);
            }
            else if (Physics > 70 && Chemistry > 70 && Maths > 70) {
                bio.add(roll);
            }
            if (Maths > 80) {
                comm.add(roll);
            }
        }
        deptwise.put("CSE",cse);
        deptwise.put("Biology",bio);
        deptwise.put("Commerce",comm);
        for (Map.Entry<String,ArrayList<Integer>> entry : deptwise.entrySet())
            System.out.println("Key = " + entry.getKey() +
                    ", Value = " + entry.getValue());

        Collections.sort(ranks, new Sortbyroll());
        for (int i=0; i<ranks.size(); i++)
            System.out.println(ranks.get(i));

    }
}
